export default {
  props: {
    clearable: {
      type: Boolean,
      required: false,
      default: false
    },
    resetable: {
      type: Boolean,
      required: false,
      default: false
    },
    value: {
      type: String,
      default: ''
    }
  },
  data() {
    return {
      initialValue: this.value,
      isClearable: false,
      localValue: this.value
    }
  },
  methods: {
    calcIsClearable() {
      if (this.clearable && this.localValue !== '') {
        return true
      }

      return false
    },
    clear() {
      this.localValue = ''
    },
    notifyInput() {
      this.$emit('input', this.localValue)
    },
    reset() {
      this.localValue = this.initialValue
    }
  },

  render() {
    return this.$scopedSlots.default({
      isClearable: this.calcIsClearable(),
      onClear: () => {
        this.localValue = ''
        this.notifyInput()
      },
      onInput: e => {
        this.localValue = e.target.value
        this.notifyInput()
      },
      value: this.localValue
    })
  }
}
